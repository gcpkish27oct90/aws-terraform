#!/usr/bin/env bash

preRequisitecheck(){
  echo "Pre-requiste check for terraform AWS"
  which terraform
  exitCode=$?
  if [[ $exitCode -ne 0 ]]; then
    #statements
    sleep 10s
    echo "terraform setup not done on this server and kindly made setup. Thank You"
    exit 1
  fi
  if [ ! -d "$JENKINS_HOME/workspace/Terraform/EC2/.terraform" ]
  then
    echo "terraform environment for available for EC2 Creation & initiating it"
    echo "Copying EC2 instance terraform code to Terraform environment"
    cp -rp /var/lib/jenkins/workspace/aws-terraform_development/EC2 $JENKINS_HOME/workspace/Terraform/
    cd $JENKINS_HOME/workspace/Terraform/EC2
    terraform init
  fi
}

terraformExecution(){
  echo "EC2 Creation initiating"
  cd $JENKINS_HOME/workspace/Terraform/EC2
  #terraform plan -var-file="ec2"
  #echo "AWS_PRFOILE=$AWS_PROFILE"
  #export AWS_PROFILE
  terraform show
  sleep 3s 
  terraform plan -var-file=ec2.tfvars -out=ec2plan.plan -state=ec2_terraform.tfstate
  sleep 2s
  terraform apply -input=true -var-file=ec2.tfvars -auto-approve -state=ec2_terraform.tfstate
  exitCode=$?
  echo "exitCode=$exitCode"
  if [ $exitCode != 0 ]
  then
    echo "Issue on Terraform execution..."
    exit 1
  fi
}
preRequisitecheck
terraformExecution
echo "terraform execution successfully fcompleted"

