provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
  shared_credentials_file = "${var.shared_credentials_file}"
}

resource "aws_lambda_function" "lambda_function" {
  role             = "${var.role_subscriber}"
  handler          = "${var.handler_name}"
  runtime          = "${var.runtime}"
  filename         = "${var.filename}"
  function_name    = "${var.function_name}"
  source_code_hash = "${base64sha256(file("${var.filename}"))}"
  vpc_config {
    subnet_ids = "${var.subscriber_subnet_ids}"
    security_group_ids = "${var.subscriber_security_group_ids}"
   }

}
