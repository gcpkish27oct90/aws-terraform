variable "runtime" {}
variable "function_name" {}
variable "handler_name" {}
variable "profile"{}
variable "role_subscriber" {}
variable "filename" {}
variable "region" {}
variable "shared_credentials_file" {}
variable "subscriber_subnet_ids"{
type = "list"
description = "Subscriber subnet ids"
}
variable "subscriber_security_group_ids" {
 type = "list"
 description = "Subscriber security groupt ids"
}
