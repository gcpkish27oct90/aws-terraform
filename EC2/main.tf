provider "aws" {
region = "${var.region}"
shared_credentials_file = "${var.shared_credentials_file}"
profile = "${var.profile}"
}

resource "aws_instance" "ec2" {
ami = "${var.ami}"
instance_type = "${var.instance_type}"
tags {
Name = "${var.tag_name}"
}
key_name = "${aws_key_pair.ec2-instances-key.key_name}"

}

resource "aws_key_pair" "ec2-instances-key" {
key_name = "ec2-instances"
public_key = "${file("${var.aws_ssh_ec2_key_file}.pub")}"
}

resource "null_resource" "OnStart-Ec2-setup" {
    connection {
          user = "ec2-user"
          host = "${aws_instance.ec2.public_ip}"
          agent = false
          private_key = "${file("/var/lib/jenkins/Keys/ec2-instances")}"
        }
 provisioner "remote-exec" {
    inline = [ "rm /home/ec2-user/decrypt.py",
      " ls -lrt ","sudo su - <<EOF",
      "yum install httpd -y",
    ]
  }
 }

resource "null_resource" "OnStart-fileupdate" {
     connection {
          user = "ec2-user"
          host = "${aws_instance.ec2.public_ip}"
          agent = false
          private_key = "${file("/var/lib/jenkins/Keys/ec2-instances")}"
        }
 provisioner "file" {
       source      = "/var/lib/jenkins/workspace/Terraform/Files/decrypt.py"
    destination = "/home/ec2-user/decrypt.py"
 }
}
