variable "region" {}
variable "shared_credentials_file" {}
variable "ami" {}
variable "instance_type" {}
variable "profile" {}
variable "tag_name" {}
variable "aws_ssh_ec2_key_file" {}
variable "tag_name-test" {}
